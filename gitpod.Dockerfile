FROM gitpod/workspace-full:latest

# Install ligo
RUN export RUNLEVEL=1
RUN wget 'https://gitlab.com/ligolang/ligo/-/jobs/artifacts/dev/download?job=docker_extract' -O ligo.zip && unzip ligo.zip ligo
RUN chmod +x ./ligo
RUN sudo cp ./ligo /usr/local/bin

# Install poetry, pytezos dependencies and python version 
RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
ENV PATH "$PATH:~/.poetry/bin"
RUN sudo apt-get install -y libsodium-dev libsecp256k1-dev libgmp-dev
RUN pyenv install 3.9.10
RUN pyenv local 3.9.10

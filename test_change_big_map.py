from os.path import dirname, join
from unittest import TestCase

from pytezos import ContractInterface
from pytezos.context.impl import ExecutionContext
from pytezos.rpc import RpcNode, ShellQuery
from pytezos.crypto.key import Key

ALICE_KEY = 'edsk3QoqBuvdamxouPhin7swCvkQNgq4jP5KZPbwWNnwdZpSpJiEbq'


class Test(TestCase):
    def test_deployed_change_big_map(self):
        context = ExecutionContext(
            shell=ShellQuery(RpcNode('http://localhost:20000')),
            key=Key.from_encoded_key(ALICE_KEY),
        )
        self.change_contract = ContractInterface.from_file(
            join(dirname(__file__), './change_big_map.mligo.tz'),
            context,
        )
        result = self.change_contract.originate(initial_storage={}).send(min_confirmations=1)
        originated_address = result.opg_result['contents'][0]['metadata']['operation_result']['originated_contracts'][0]
        self.change_contract.context.address = originated_address  # without this can't call entrypoint

        with self.assertRaises(KeyError):
            self.change_contract.storage['bar']()
        self.change_contract.default('bar').send(min_confirmations=1)
        self.assertEqual(self.change_contract.storage['bar'](), 1)

    def test_executed_change_big_map(self):
        context = ExecutionContext(
            shell=ShellQuery(RpcNode('http://localhost:20000')),
            key= Key.from_encoded_key(ALICE_KEY),
        )
        self.change_contract = ContractInterface.from_file(
            join(dirname(__file__), './change_big_map.mligo.tz'),
            context,
        )
        result = self.change_contract.default('qwerty').run_code(storage={'some': 8})
        self.assertEqual(0, len(result.operations))
        print('result.storage', result.storage)
        print('result.lazy_diff', result.lazy_diff)
        self.assertEqual(result.lazy_diff, [{'qwerty': 1}])

    def test_interpret_change_big_map(self):
        self.change_contract = ContractInterface.from_file(
            join(dirname(__file__), './change_big_map.mligo.tz'),
        )
        result = self.change_contract.default('qwerty').interpret(storage={'some': 8})
        self.assertEqual(0, len(result.operations))
        self.assertEqual(result.lazy_diff, [{
            'diff': {
                'action': 'alloc',
                'key_type': {'prim': 'string'},
                'updates': [{'key': {'string': 'qwerty'},
                                'key_hash': 'exprv5ZgxKrPsCQLWurTYwcXDjiaLXUDLUmq4XsFjt6uJt2EFBHFUV',
                                'value': {'int': '1'}},
                            {'key': {'string': 'some'},
                                'key_hash': 'expruCfHW9jxvMte8YTbNc66o9chaE5G77hMk7n6KMzPQbdnzMH8zr',
                                'value': {'int': '8'}}],
                'value_type': {'prim': 'nat'}},
            'id': '0',
            'kind': 'big_map',
        }])
        self.assertEqual(result.storage, {'qwerty': 1, 'some': 8})

#!/usr/bin/env bash
image=oxheadalpha/flextesa:latest
script=hangzbox

# macos users: https://gitlab.com/tezos/flextesa/blob/master/README.md#macosx-users
# export PATH="/usr/local/opt/coreutils/libexec/gnubin:/usr/local/opt/util-linux/bin:$PATH"

docker run --rm --name test_sandbox --detach -p 20000:20000 \
       -e block_time=2 \
       "$image" "$script" start

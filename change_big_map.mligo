type storage = (string, nat) big_map
type parameter = string
type result = operation list * storage

let main (param, storage : parameter * storage) : result =
  let new_storage = Big_map.update param (Some(1n)) storage in
  (([] : operation list), new_storage )

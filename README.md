# Demo code

Run tests `pytest . -v`

## Description

Can't check big map changes in contract execution in `test_change_big_map.py`.

## Compile ligo

    ligo compile contract change_big_map.mligo -e main --output-file change_big_map.mligo.tz -p hangzhou
